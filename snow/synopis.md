# Synopsis

David Jensen, an ordinary guy in an ordinary world. He seemed ordinary, but behind the cracks of his carefully constructed facade, paradoras box was tucked away, as far away from sunlight as possible.

David had always seemed the calm guy that did his job and was like everybody else, average. He didn't seem to have any remarkable features. But where normal people would perform better at some things than others. He always seemed to be just average, never deviating from his normalcy, always at a steady median. This made him a shadow among others. He did have friends, but none too close.

A chance encouter and a deviated destiny made him something that he shouldn't have been. An irregularity, someone touched personally by Chaos and then driven by Fate.

Tags:

Action, Adventure, Romance, Harem, Anti-Hero, Gore, Sexual-Content, Strong-Lead, Traumatic, Tragic.
